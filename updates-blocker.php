<?php
/*
Plugin Name: Plugin Update Blocker
Plugin URI: https://bitbucket.org/sf-repos/wordpress-plugin-update-blocker-mu-plugin/
Description: Blocks updates for the plugins listed in this file.
Version: 1.0
Author: Sam Fullalove
Author URI: http://sam.fullalove.co/
Text Domain: ls-plugin-update-blocker
License: GPLv2
*/

add_filter( 'site_transient_update_plugins', 'ls_disable_plugin_updates' );
/**
* Blocks updates for the plugins listed in the contained array
*/
function ls_disable_plugin_updates( $value ) {

  $blocked_plugins = array(
      'our-team/our-team.php',
      'portfolio/portfolio.php',
      'my-reviews/my-reviews.php',
  );

  foreach( $blocked_plugins as $p ) {
      if(isset($value->response[$p])){
        unset( $value->response[$p]);
      }
  }

   return $value;
}

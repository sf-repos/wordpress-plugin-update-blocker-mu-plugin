# update blocker

If wordpress finds a plug-in with the same name as yours, it will update your plug-in with another one...which breaks your plug-in!

By adding your plug-in folder and php file to this plugin, it will ensure that your plug-in is not updated by wordpress.

To use: 

1: put in the "mu-plugins" folder (if you don't have one under wp-content, then make one)

2: open the updates-blocker.php find this array and replace those plug-ins with yours:

 $blocked_plugins = array(
      'our-team/our-team.php',
      'portfolio/portfolio.php',
      'my-reviews/my-reviews.php',
  );